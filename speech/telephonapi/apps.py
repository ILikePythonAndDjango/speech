from django.apps import AppConfig


class TelephonapiConfig(AppConfig):
    name = 'telephonapi'
